@extends('layout.master')
@section('judul')
  Halaman Pendaftaran
@endsection

@section('isi')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
      <form action="/welcome" method="post">
          @csrf
          <legend>First Name: </legend><br>
            <input type="text" name="namadepan"><br><br>
          <legend>Last Name: </legend><br>
            <input type="text" name="Nama Belakang"><br><br>
          <legend>Gender: </legend><br>
            <input type="radio" name="Laki-Laki">Male<br>
            <input type="radio" name="Perempuan">Female<br>
            <input type="radio" name="Lainnya">Other<br><br>
          <legend>Nationality: </legend><br>
            <select>
              <option value="Indonesian">Indonesian</option><br>
              <option value="Foreign">Foreign</option><br>
            </select><br>
          <br><legend>Language Spoken: </legend><br>
            <input type="checkbox" name="Bahasa Indonesia">Bahasa Indonesia<br>
            <input type="checkbox" name="English">English<br>
            <input type="checkbox" name="Other">Other<br><br>
          <label>Bio: </label><br><br>
            <textarea name="Bio" rows="10" cols="30"></textarea><br>
            <input type="submit" name="Sign Up" value="Sign Up">
      </form>
@endsection
    
