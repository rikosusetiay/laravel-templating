@extends('layout.master')
@section('judul')
  Detail Cast {{$cast->nama}}
@endsection

@section('isi')

<h1>{{$cast->nama}}</h1>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary">Kembali</a>

@endsection